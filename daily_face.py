import decimal
import os
from datetime import datetime

import matplotlib.pyplot as plt
from PyQt5 import QtWidgets
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QIcon
from loguru import logger

from data_module import DataModule
from ui_daily import Ui_MainWindow

DAY_H = datetime(2022, 1, 29, 0, 0, 0, 849982)


class DailyFace(QtWidgets.QMainWindow):
    def __init__(self):
        """ Class constructor.
        """
        # FORMAT = '%(asctime)s: %(name)s: %(levelname)s: %(message)s'  TODO: этот блок для случая logging.
        # filename = str(os.path.dirname(os.path.abspath(__file__))) + '/daily.log'
        # logging.basicConfig(filename=filename, level=logging.INFO,
        #                     format=FORMAT, datefmt='%Y-%m-%d %H:%M:%S')
        # self.logger = logging.getLogger(__name__)

        super(DailyFace, self).__init__()
        self.dm = DataModule()
        self.current_day = {}
        self.current_thought = -10  # Current thought is undefined
        self.setWindowIcon(QIcon(os.path.dirname(os.path.abspath(__file__)) +
                                 '/logo.png'))
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.tag_list_model = QStandardItemModel(self.ui.tag_list)
        self.all_tags = self.dm.get_all_tags()
        self.setWindowTitle('Daily')
        self.ui.dateEdit.setDate(datetime.now())
        self.statusBar().showMessage('Today: ' + self.get_date())

        self.ui.alignButton.clicked.connect(self.collect_info)
        self.ui.submit_button.clicked.connect(self.submit)
        self.ui.upButton.clicked.connect(self.up_button_pressed)
        self.ui.downButton.clicked.connect(self.down_button_pressed)
        self.ui.push_ups_ledt.returnPressed.connect(self.collect_info)
        self.ui.squates_ledt.returnPressed.connect(self.collect_info)
        self.ui.weight_ledt.returnPressed.connect(self.collect_info)
        self.ui.steps_ledt.returnPressed.connect(self.collect_info)
        self.ui.sleeptime_ledt.returnPressed.connect(self.collect_info)
        self.ui.deep_sleeptime_ledt.returnPressed.connect(self.collect_info)
        self.ui.temperature_ledt.returnPressed.connect(self.collect_info)
        self.ui.tabWidget.setCurrentIndex(0)
        self.ui.tabWidget.currentChanged[int].connect(self.collect_info)
        self.ui.graphButton.setText('Charts')
        self.ui.graphButton.clicked.connect(self.achievements_charts)
        self.ui.sleepButton.clicked.connect(self.sleeptime_charts)
        # self.ui.refreshButton.clicked.connect(self.renewal)

        self.renewal()
        logger.debug('class daily_face initialized.')

    def num2str(self, numeric: int) -> str:
        last_digit = str(numeric)[-1]
        if last_digit == '1':
            numeral = 'день'
        elif last_digit in ['2', '3', '4']:
            numeral = 'дня'
        else:
            numeral = 'дней'
        return str(numeric) + ' ' + numeral

    def get_date(self) -> str:
        """ Get desired date.
            OUTPUT: <str> - desired date.
        """
        return datetime.strftime(self.ui.dateEdit.date().toPyDate(), "%Y-%m-%d")

    def renewal(self) -> None:
        """ Renewal the screen.
        """
        desired_date = self.get_date()
        self.current_day = self.dm.get_day(desired_date)
        self.current_thought = -10
        logger.info('On this day there were ' + str(len(self.current_day['thoughts'])) + ' thoughts.')
        bmi_displayed_value = '(' + '<font color="#FF0000">' + str(self.current_day['bmi']) + '</font>) ' if float(
            self.current_day['bmi']) >= 25 else '(' + str(self.current_day['bmi']) + ') '

        dss_displayed_value = '<font color="#FF0000">' + str(self.current_day['dss']) + '</font>) ' if float(
            self.current_day['dss'][:-1]) < 20 else str(
            self.current_day['dss'])  # + '%'

        temperature_value = '<font color="#FF0000">' + str(self.current_day['temperature']) + '</font>) ' if float(
            self.current_day['temperature']) >= 37.3 else str(self.current_day['temperature'])

        now = datetime.now()
        delta = now - DAY_H
        days_left = '<font color="#00FF00">' + str(delta.days)
        logger.info('Days have passed since ' + DAY_H.strftime("%d.%m.%Y") + ': ' + str(delta.days))

        self.ui.mainEdit.clear()
        self.ui.mainEdit.append(
            '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *')
        self.ui.mainEdit.append('<B>' + str(self.current_day['date']) + ' ' +  # date
                                str(self.current_day['push_ups']) + ' ' +  # push_ups
                                str(self.current_day['squates']) + ' ' +  # squats
                                str(self.current_day['weight']) + ' ' +  # weight
                                bmi_displayed_value +  # bmi
                                str(self.current_day['steps']) + ' (' +  # steps
                                str(self.current_day['sleeptime']) + '/' +  # sleeptime
                                str(self.current_day['deep_sleeptime']) + ' ' +  # deep_sleeptime
                                dss_displayed_value +  # Deep Sleep Share
                                ') ' +
                                temperature_value + ' ' +   # temperature
                                days_left +   # days left
                                '</B>')
        self.ui.mainEdit.append(
            '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *')

        self.ui.textEdit.clear()
        self.ui.thought_edit.clear()

        self.ui.tag_list.setModel(self.tag_list_model)
        self.tag_list_model.clear()

        self.all_tags = self.dm.get_all_tags()
        for tag_list_row in self.all_tags:
            item = QStandardItem(tag_list_row)
            item.setCheckable(True)
            # item.setCheckState(2)  # 2-enabled, 1-non defined, 0-disabled.
            self.tag_list_model.appendRow(item)

        if self.current_day['thoughts']:
            self.current_day['thoughts'].sort(key=lambda x: x['timestamp'])
            for thought in self.current_day['thoughts']:
                self.ui.textEdit.append(f"<I>{thought['timestamp'][-8:-3]}</I> {thought['record']}")
                tags = []
                for tag in thought['tags']:
                    tags.append(tag['tag'])
                self.ui.textEdit.append('<font color="#606060"><font size="80%"><I>' +
                                        str(tags).strip("[]").replace("'", "") +
                                        '</I></font>')  # strip to exclude [], a replace - '.

    def collect_info(self) -> None:
        """ Collect entered data
        """
        current_date = self.get_date()
        new_day = self.dm.get_day(current_date)
        if self.ui.push_ups_ledt.text():
            new_day['push_ups'] = str(int(new_day['push_ups']) +
                                      int(self.ui.push_ups_ledt.text()))  # push_ups
        if self.ui.squates_ledt.text():
            new_day['squates'] = str(int(new_day['squates']) +
                                     int(self.ui.squates_ledt.text()))  # squates
        if self.ui.weight_ledt.text():
            new_day['weight'] = self.ui.weight_ledt.text().replace(',', '.')  # weight
        if self.ui.steps_ledt.text():
            new_day['steps'] = self.ui.steps_ledt.text()  # steps
        if self.ui.sleeptime_ledt.text():
            delta = self.ui.sleeptime_ledt.text()
            if len(delta.split(':')) == 2:
                delta += ':00'
            new_day['sleeptime'] = delta  # sleeptime
        if self.ui.deep_sleeptime_ledt.text():
            delta = self.ui.deep_sleeptime_ledt.text()
            if len(delta.split(':')) == 2:
                delta += ':00'
            new_day['deep_sleeptime'] = delta  # deep_sleeptime
        if self.ui.temperature_ledt.text():
            new_day['temperature'] = self.ui.temperature_ledt.text().replace(',', '.')  # temperature

        if not (self.dm.put_day(current_date, new_day)):
            logger.exception('Something went wrong. Possibly incorrect data. Data not saved.')
            # sys.exit()

        logger.info('Data successfully sent.')
        self.ui.push_ups_ledt.clear()
        self.ui.squates_ledt.clear()
        self.ui.weight_ledt.clear()
        self.ui.steps_ledt.clear()
        self.ui.sleeptime_ledt.clear()
        self.ui.deep_sleeptime_ledt.clear()
        self.ui.temperature_ledt.clear()

        self.renewal()

    def submit(self) -> bool:
        """ Add new thought of the day.
            OUTPUT: <bool> - True, if thought was added successfully.
        """
        if not self.ui.thought_edit.toPlainText():  # thought_edit is empty
            logger.error('Trying to keep an empty thought.')
            return

        if not self.current_thought == -10:  # not new thought
            logger.info('Thought changed. New record is "' +
                        str(self.ui.thought_edit.toPlainText()) + '".')

            # Change thought record
            self.current_day['thoughts'][self.current_thought]['record'] = str(self.ui.thought_edit.toPlainText())
        else:
            desired_date = self.get_date()
            self.current_day = self.dm.get_day(desired_date)

        desired_date = self.get_date()
        selected_tags = {}
        for row in range(self.tag_list_model.rowCount()):
            if self.tag_list_model.item(row).checkState():
                selected_tags[row + 1] = self.tag_list_model.item(row).text()

        tags = []
        for i in selected_tags.keys():
            tags.append({'id': i})

        if not tags:
            logger.error('No tags selected. Data will not be saved.')
            return

        if self.current_thought == -10:  # new thought
            self.current_day['thoughts'].append({
                'daily': self.current_day['id'],
                'timestamp': datetime.today().strftime("%Y-%m-%dT%H:%M:%S"),  # Current date/time
                'record': self.ui.thought_edit.toPlainText(),
                'tags': tags
            })
        else:
            # Add tags to 'day' object
            self.current_day['thoughts'][self.current_thought]['tags'] = tags
        self.dm.put_day(desired_date, self.current_day)

        self.ui.thought_edit.clear()
        self.tag_list_model.clear()

        self.renewal()
        return

    def up_button_pressed(self) -> None:
        """ upButton pressed
        """
        if len(self.current_day['thoughts']) == 0:  # Not thoughts at that day yet
            return

        if self.current_thought == -10:  # Current thought is undefined
            self.current_thought = len(self.current_day['thoughts']) - 1  # The last thought on the list becomes current
        else:
            self.current_thought -= 1 if self.current_thought >= 1 else 0  # Either the previous or the first

        # Display current thought in thought_edit
        self.ui.thought_edit.clear()
        self.ui.thought_edit.append(self.current_day['thoughts'][self.current_thought]['record'])

        self.current_day['thoughts'].sort(key=lambda x: x['timestamp'])  # sort by timestamp

        # Mark tags of current thought
        tags = []
        for tag in self.current_day['thoughts'][self.current_thought]['tags']:
            tags.append(tag['tag'])

        self.tag_list_model.clear()
        for tag_list_row in self.all_tags:
            item = QStandardItem(tag_list_row)
            item.setCheckable(True)
            item.setCheckState(2) if tag_list_row in tags else item.setCheckState(0)
            self.tag_list_model.appendRow(item)

    def down_button_pressed(self) -> None:
        if len(self.current_day['thoughts']) == 0:  # Not thoughts at that day yet
            return

        if self.current_thought == -10:  # Current thought is undefined
            self.current_thought = 0  # The first thought on the list becomes current
        else:
            if self.current_thought < (len(self.current_day['thoughts']) - 1):
                self.current_thought += 1  # The current becomes the next
            else:
                self.current_thought = len(self.current_day['thoughts']) - 1  # or the last one remains

        # Display current thought in thought_edit
        self.ui.thought_edit.clear()
        self.ui.thought_edit.append(self.current_day['thoughts'][self.current_thought]['record'])

        # Отметить теги текущей мысли
        self.current_day['thoughts'].sort(key=lambda x: x['timestamp'])

        tags = []
        for tag in self.current_day['thoughts'][self.current_thought]['tags']:
            tags.append(tag['tag'])

        self.tag_list_model.clear()
        for tag_list_row in self.all_tags:
            item = QStandardItem(tag_list_row)
            item.setCheckable(True)
            item.setCheckState(2) if tag_list_row in tags else item.setCheckState(0)
            self.tag_list_model.appendRow(item)

    def achievements_charts(self) -> None:
        """ Plotting achievements charts
        INPUT: None
        OUTPUT: None
        """
        days = self.dm.get_last_days()
        data = [[day['date'], day['weight'], day['steps'], day['squates'], day['push_ups']] for day in days]
        x, y1, y2, y2r, y3, y4 = [], [], [], [], [], []
        for row in data:
            x.append(datetime.strptime(row[0], '%Y-%m-%d'))  # date
            y1.append(decimal.Decimal(str(row[1])))  # weight

            # y2 - green, if the norm is fulfilled, и y2r - pink, if not.
            toadd = int(row[2])  # steps
            if toadd < 10000:  # daily norm of steps
                y2.append(0)
                y2r.append(toadd)
            else:
                y2.append(toadd)
                y2r.append(0)

            y3.append(row[3])  # squates
            y4.append(row[4])  # push_ups
        # steps (green, pink) y2, y2r
        ax1 = plt.subplot(2, 2, 1)
        ax1.bar(x, y2, color='green')  # steps >= 10000
        ax1.bar(x, y2r, color='pink')  # steps < 10000
        plt.grid(True)
        ax1.set_title('Steps', fontsize=10)
        ax1.set_xlabel('Date', fontsize=8)
        ax1.set_ylabel('Steps number', fontsize=8)

        # weight (red) y1
        ax2 = plt.subplot(2, 2, 2)
        ax2.plot(x, y1, 'r-')
        ax2.set_title('Weight', fontsize=10)
        plt.grid(True)
        ax2.set_xlabel('Date', fontsize=8)
        ax2.set_ylabel('Weight (kg)', fontsize=8)

        # squates (orange) y3
        ax3 = plt.subplot(2, 2, 3)
        ax3.set_title('Squats', fontsize=10)
        plt.grid(True)
        ax3.bar(x, y3, color='orange')
        ax3.set_xlabel('Date', fontsize=8)
        ax3.set_ylabel('Squats number', fontsize=8)

        # steps (blue) y4
        ax4 = plt.subplot(2, 2, 4)
        ax4.set_title('Push_ups', fontsize=10)
        plt.grid(True)
        ax4.bar(x, y4, color='blue')
        ax4.set_xlabel('Date', fontsize=8)
        ax4.set_ylabel('Push_ups number', fontsize=8)

        plt.setp(ax1.get_xticklabels(), rotation=30, ha='right', fontsize=8)
        plt.setp(ax2.get_xticklabels(), rotation=30, ha='right', fontsize=8)
        plt.setp(ax3.get_xticklabels(), rotation=30, ha='right', fontsize=8)
        plt.setp(ax4.get_xticklabels(), rotation=30, ha='right', fontsize=8)
        plt.setp(ax1.get_yticklabels(), fontsize=8)
        plt.setp(ax2.get_yticklabels(), fontsize=8)
        plt.setp(ax3.get_yticklabels(), fontsize=8)
        plt.setp(ax4.get_yticklabels(), fontsize=8)

        plt.tight_layout()
        plt.gcf().canvas.set_window_title('Achievements charts')
        plt.show()
        return

    def sleeptime_charts(self) -> None:
        filename = str(os.path.dirname(os.path.abspath(__file__)))
        logger.info(filename + '/sleeptime.py')
        os.system(filename + '/sleeptime.py')
        return
