#!/usr/bin/python3
# -*- coding: utf-8 -*-
""" Sleeptime charts
"""

from datetime import datetime

import matplotlib.pyplot as plt
import requests

days = requests.get('http://lifedesc/last/').json()
data = [[day['date'], day['sleeptime'], day['deep_sleeptime']] for day in days]
x, y1, y2 = [], [], []
for row in data:
    x.append(datetime.strptime(row[0], '%Y-%m-%d'))  # date
    y1.append(int(row[1].split(':')[0]) + int(row[1].split(':')[1]) / 60)  # sleeptime
    y2.append(int(row[2].split(':')[0]) + int(row[2].split(':')[1]) / 60)  # deep_sleeptime

ax1 = plt.subplot(1, 1, 1)
ax1.set_title('Sleep duration', fontsize=10)
plt.grid(True)
ax1.bar(x, y1, color='aqua')
ax1.bar(x, y2, color='blue')
ax1.set_xlabel('Date', fontsize=8)
ax1.set_ylabel('Total/deep', fontsize=8)
plt.setp(ax1.get_xticklabels(), rotation=30, ha='right', fontsize=8)
plt.setp(ax1.get_yticklabels(), fontsize=8)
plt.tight_layout()
plt.gcf().canvas.set_window_title('Sleep duration')

plt.show()
