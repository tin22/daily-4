#!/home/tin/VE/Daily-4/bin/python3
# _*_ coding: utf-8 _*_

import atexit
import sys
from pathlib import Path

from PyQt5 import QtWidgets
from loguru import logger

import daily_face

logger.add(Path(__file__).parent / 'daily.log',
           format="{time:YYYY-MM-DD at HH:mm:ss} | {level} | {module} | {message}",
           level="INFO")

logger.info('==== Program started. ====')  # Executing interpreter: {sys.executable}')


def endlog():
    logger.info('==== Program ended. ==== \n')


atexit.register(endlog)

if __name__ == '__main__':
    logger.debug('Main loop in progress.')
    app = QtWidgets.QApplication(sys.argv)
    application = daily_face.DailyFace()
    application.show()
    logger.debug('The program is ready and running')
    sys.exit(app.exec())
